# Aplikasi Digital Library STMIK Sumedang
![Web_capture_16-1-2021_16486_127.0.0.1](/uploads/bcc38644b3dbfec5ccb896a0a67418f6/Web_capture_16-1-2021_16486_127.0.0.1.jpeg)

# Akun
Admin
Email       : admin@admin.com <br>
Password    : 123456
<br>
User
Email       : user@user.com <br>
Password    : 123456

# Video Testing Aplikasi
https://youtu.be/zgi8_GEcxaI

# Note
Jika Gambar Tidak Muncul Hapus Folder Storage di Folder Public Lalu Jalankan : <br>
php artisan storage:link <br>
Import Data DB Dengan File dlstmik.sql

